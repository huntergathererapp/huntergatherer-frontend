FROM sbtscala/scala-sbt:11.0.15_1.7.1_3.1.3

COPY [".", "/home/app"]

WORKDIR /home/app

EXPOSE 9000