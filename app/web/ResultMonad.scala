package web.resultMonad;

import logic.validation._;
import play.api.libs.json._;
import play.api.libs.ws.WSResponse;
import play.api.mvc.Results;
import scala.concurrent.ExecutionContext;
import scala.concurrent.Future;

sealed trait ResultMonad[T]
{
  def flatMap[TNext](f: T => ResultMonad[TNext]): ResultMonad[TNext];
  def map[TNext](f: T => TNext): ResultMonad[TNext];
  def toResult(f: T => play.api.mvc.Result): play.api.mvc.Result;
};

/*----------------------companion object -----------------------------------*/
object ResultMonad
{
  implicit val ec: ExecutionContext = ExecutionContext.global;
  def fromJsResult[T](value: JsResult[T]): ResultMonad[T] =
  {
    value match
    {
      case JsSuccess(model, _) => new Ok[T](model);
      case e: JsError => new BadRequest[T](JsError.toJson(e));
    };
  };

  def fromValidated[T](value: Validated[T]): ResultMonad[T] =
  {
    value match
    {
      case Valid(model) => new Ok[T](model);
      case Invalid(errorCodes) => new BadRequest[T](Json.toJson(errorCodes));
    }
  };

  //HACK this case/match is simplistic. It covers the cases I forsee at this
  //point and treats anything else as an error.
  def fromWSResponse(response: WSResponse): ResultMonad[JsValue] =
  {
    response.status match
    {
      case x if x >= 200 && x < 300 => new Ok(response.json);
      case x if x >= 400 && x < 500 => new BadRequest(response.json);
      case _ => new Error(response.json);
    };
  };

  def invertFuture[T](monad: ResultMonad[Future[T]]): Future[ResultMonad[T]] =
  {
    monad match
    {
      case ok: Ok[Future[T]] => ok.t map (member => new Ok(member));
      case bad: BadRequest[Future[T]] =>
        Future { new BadRequest[T](bad.errorJson) };
      case error: Error[Future[T]] =>
        Future { new Error[T](error.errorJson); };
      case forbidden: Forbidden[Future[T]] =>
        Future { new Error[T](forbidden.errorJson) };
    };
  };
};

/*------------------------------implementations----------------------------*/

class BadRequest[T](errors: JsValue) extends ResultMonad[T]
{
  def errorJson: JsValue = errors;
  def flatMap[TNext](f: T => ResultMonad[TNext]): ResultMonad[TNext] =
  {
    new BadRequest[TNext](errors);
  };
  
  def map[TNext](f: T => TNext): ResultMonad[TNext] =
  {
    new BadRequest[TNext](errors);
  };

  def toResult(f: T => play.api.mvc.Result): play.api.mvc.Result =
    play.api.mvc.Results.InternalServerError(errors);
};

class Error[T](errors: JsValue) extends ResultMonad[T]
{
  def errorJson: JsValue = errors;
  def flatMap[TNext](f: T => ResultMonad[TNext]): ResultMonad[TNext] =
  {
    new Error[TNext](errors);
  };

  def map[TNext](f: T => TNext): ResultMonad[TNext] =
  {
    new Error[TNext](errors);
  };

  def toResult(f: T => play.api.mvc.Result): play.api.mvc.Result =
    play.api.mvc.Results.BadRequest(errorJson);
};

class Forbidden[T](errors: JsValue) extends ResultMonad[T]
{
  def errorJson: JsValue = errors;
  def flatMap[TNext](f: T => ResultMonad[TNext]): ResultMonad[TNext] =
  {
    new Forbidden[TNext](errors);
  };

  def map[TNext](f: T => TNext): ResultMonad[TNext] =
  {
    new Forbidden[TNext](errors);
  };

  def toResult(f: T => play.api.mvc.Result): play.api.mvc.Result =
    play.api.mvc.Results.Forbidden(errorJson);

}

class Ok[T](member: T) extends ResultMonad[T]
{
  def flatMap[TNext](f: T => ResultMonad[TNext]): ResultMonad[TNext] =
  {
    f(member);
  };

  def map[TNext](f: T => TNext): ResultMonad[TNext] =
  {
    new Ok[TNext](f(member));
  };

  def toResult(f: T => play.api.mvc.Result): play.api.mvc.Result = f(t);

  /*-------------------------implementation-specific members-----------------*/
  def t: T = member;
};

