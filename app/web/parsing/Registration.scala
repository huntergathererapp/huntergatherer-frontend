package web.parsing;

import play.api.libs.json._;
import scala.util.chaining._;
import web.resultMonad.ResultMonad;
import web.parsing.Parsers;

object Registration
{
  def toModel(jsValue: JsValue): ResultMonad[web.models.Registration] =
  {
    (jsValue.validate[web.models.Registration](Parsers.registration)) pipe
    (jsResult => ResultMonad.fromJsResult(jsResult))
  };
}
