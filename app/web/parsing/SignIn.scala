package web.parsing;

import play.api.libs.json._;
import scala.util.chaining._;
import web.resultMonad.ResultMonad;
import web.parsing.Parsers;

object SignIn
{
  def toModel(jsValue: JsValue): ResultMonad[web.models.SignIn] =
  {
    (jsValue.validate[web.models.SignIn](Json.reads[web.models.SignIn])) pipe
    (jsResult => ResultMonad.fromJsResult(jsResult))
  };
}
