package logic.validation;

sealed abstract class Validated[Model];
case class Valid[Model](m: Model) extends Validated[Model];
case class Invalid[Model](errorCodes: List[String]) extends Validated[Model];
