package controllers

import javax.inject._
import play.api._
import play.api.mvc._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class JamController @Inject()(val controllerComponents: ControllerComponents,
  assets: Assets) extends BaseController
{

  /**
    * Serves (elm app) homepage. (Elm app itself has routes, including 
    * NotFound). The String parameter is "ignored" because elm take care of its
    * own routing.
    */

  def any(ignored: String) = assets.at("/public", "/index.html");
}
