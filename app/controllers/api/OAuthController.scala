package controllers.api;

import javax.inject._;
import play.api._;
import play.api.libs.json._;
import play.api.libs.ws._;
import play.api.mvc._;
import scala.concurrent.ExecutionContext;
import scala.concurrent._;
import scala.util.chaining._;
import web._
import web.cookie.HgSessionCookie;
import web.models._;
import web.parsing.PkceOAuthRequest;

@Singleton
class OAuthController @Inject()(
    configuration: Configuration,
    val controllerComponents: ControllerComponents
  ) extends BaseController
{
  def getState = Action
  {
    request: Request[_] =>
    {
      Conf.getInstance(configuration)
        .pipe(conf => HgSessionCookie.fromCookie(conf, request.session))
        .pipe(cs => cs.gitLabPkceOAuthRequest)
        match {
          case Some(pkceReq) =>
            web.parsing.PkceOAuthRequest.toJson(pkceReq)
              .pipe(json => Ok(json));
          case None => BadRequest;
        };
    };
  };

  def setState = Action(parse.tolerantJson)
  {
    request: Request[JsValue] =>
    {
      web.parsing.PkceOAuthRequest.toModel(request.body).toResult(pkce => {
        (HgSessionCookie.setGitLabPkceOAuthRequest(request.session, pkce))
        .pipe(cookie => Ok("success").withSession(cookie));
      });
    };
  };
}

