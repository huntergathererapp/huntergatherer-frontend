package data;

import dtos._;
import play.api.libs.json._;
import play.api.libs.ws._;
import scala.concurrent.ExecutionContext;
import scala.concurrent.Future;
import scala.util.chaining._;

object GitLab
{

  implicit val ec: ExecutionContext = ExecutionContext.global;
  val authenticate: (DatabaseConfiguration, GitLabOAuthRequest) =>
  Future[WSResponse] =
    (config, dto) =>
  {
    config.wsClient.url("https://gitlab.com/oauth/authorize")
      .addQueryStringParameters("client_id" -> dto.clientID,
        "code" -> dto.code, "grant_type" -> "authorization_code",
        "redirect_uri" -> dto.redirectURI, "code_verifier" -> dto.codeVerifier
      )
      .addHttpHeaders("Content-Type" -> "application/json")
      .get();
  };
}
